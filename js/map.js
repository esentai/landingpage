$(document).ready(function() {
    ymaps.ready(function () {
        var myMap = new ymaps.Map('map-canvas', {
            center: [43.237083, 76.901338],
            zoom: 17,
            controls: []
        });


        myPlacemark = createPlacemark([ 43.237083, 76.901338]);
        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors.disable('scrollZoom');
        myMap.controls.add(new ymaps.control.ZoomControl({options: { position: { right: 10, top: 90 }}}));
    });

    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
            iconCaption: 'поиск...'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/mapicon.png',
            iconImageSize: [64, 42],
            iconImageOffset: [-15, -50],
            draggable: false
        });
    }
});

$(document).ready(function() {
    ymaps.ready(function () {
        var myMap = new ymaps.Map('map-canvas2', {
            center: [43.202263, 76.892343],
            zoom: 17,
            controls: []
        });


        myPlacemark = createPlacemark([ 43.202263, 76.892343]);
        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors.disable('scrollZoom');
        myMap.controls.add(new ymaps.control.ZoomControl({options: { position: { right: 10, top: 90 }}}));
    });

    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
            iconCaption: 'поиск...'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/mapicon.png',
            iconImageSize: [64, 42],
            iconImageOffset: [-15, -50],
            draggable: false
        });
    }
});

$(document).ready(function() {
    ymaps.ready(function () {
        var myMap = new ymaps.Map('map-canvas3', {
            center: [43.239913, 76.918396],
            zoom: 17,
            controls: []
        });


        myPlacemark = createPlacemark([ 43.239913, 76.918396]);
        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors.disable('scrollZoom');
        myMap.controls.add(new ymaps.control.ZoomControl({options: { position: { right: 10, top: 90 }}}));
    });

    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
            iconCaption: 'поиск...'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/mapicon.png',
            iconImageSize: [64, 42],
            iconImageOffset: [-15, -50],
            draggable: false
        });
    }
});